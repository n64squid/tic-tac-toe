# Tic Tac Toe in C

This is just a little project I made to practice some C programming. The .exe can be found on the [releases page](https://gitlab.com/n64squid/tic-tac-toe/-/releases) or it can be compiled with the following code. I used GCC but other compilers should work fine too.

	gcc ttt.c -o ttt.exe -s

Have fun and thank you for playing!

## Features

* ASCII / Command line graphics
* 1-player mode versus AI
* 2-player mode in a hot seat
* Adjustable board size from 1x1 to 9x9

1x1 and 2x2 boards are trivially easy to win for the player with the first move.  
3x3 the AI will always win or tie except against one strategy. Can you find it?  
4x4 to 9x9 it is impossible to win because only one cell is needed to block a row, but 4-9 are needed to complete it.

## Future ideas

* Multiple AI players
* Win with fewer than a full row (to make bigger boards fair)